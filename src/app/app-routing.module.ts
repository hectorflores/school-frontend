import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';

import { HomeComponent } from './component/home/home.component';
import { StudentsComponent } from './component/students/students.component';
import { SignaturesComponent } from './component/signatures/signatures.component';
import { ProfessorsComponent } from './component/professors/professors.component';
import {CreateProfessorComponent} from './component/professors/create-professor/create-professor.component';
import {CreateStudentComponent} from './component/students/create-student/create-student.component';
import {CreateSignatureComponent} from './component/signatures/create-signature/create-signature.component';

const ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'students/new', component: CreateStudentComponent },
  { path: 'signatures', component: SignaturesComponent },
  { path: 'signatures/new', component: CreateSignatureComponent },
  { path: 'professors', component: ProfessorsComponent },
  { path: 'professors/new', component: CreateProfessorComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(ROUTES),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
