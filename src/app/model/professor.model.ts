export class Professor {
  id?: number;
  name?: string;
  email?: string;
  age?: number;
}
