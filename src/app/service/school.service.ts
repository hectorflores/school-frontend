import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NgForm} from '@angular/forms';
import {Student} from '../model/student.model';
import {Signature} from '../model/signature.model';
import {Professor} from '../model/professor.model';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  // API_URL = 'http://eduardoapicoe-eval-test.apigee.net/v1';
  API_URL = 'http://localhost';

  constructor(private  httpClient: HttpClient) {}

  getStudents(): Observable<any> {
    return this.httpClient.get(this.API_URL + ':8081/student');
  }

  getProfessors(): Observable<any> {
    return this.httpClient.get(this.API_URL + ':8082/professor');
  }

  getSignatures(): Observable<any> {
    return this.httpClient.get(this.API_URL + ':8084/signature');
  }

  saveStudent(student: Student) {
    let body = student;
    let headers = new HttpHeaders({
      'Content-Type':'application/json'
    });

    return this.httpClient.post(this.API_URL + ':8081/student', body, {headers: headers})
      .toPromise()
      .then(res => {console.log("Success Saved!", res)})
      .catch(err => { return Promise.reject('Student already exist' && err.error);})
  }

  saveSignature(signature: Signature) {
    let body = signature;
    let headers = new HttpHeaders({
      'Content-Type':'application/json'
    });

    return this.httpClient.post(this.API_URL + ':8084/signature', body, {headers: headers})
      .toPromise()
      .then(res => {console.log("Success Saved!", res)})
      .catch(err => { return Promise.reject('Signature already exist' && err.error);})
  }

  saveProfessor(professor: Professor) {
    let body = professor;
    let headers = new HttpHeaders({
      'Content-Type':'application/json'
    });

    return this.httpClient.post(this.API_URL + ':8082/professor', body, {headers: headers})
      .toPromise()
      .then(res => {console.log("Success Saved!", res)})
      .catch(err => { return Promise.reject('Professor already exist' && err.error);})
  }
}
