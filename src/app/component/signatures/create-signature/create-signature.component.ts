import { Component, OnInit } from '@angular/core';
import {SchoolService} from '../../../service/school.service';
import Swal from "sweetalert2";
import {Signature} from '../../../model/signature.model';

@Component({
  selector: 'app-create-signature',
  templateUrl: './create-signature.component.html',
  styleUrls: ['./create-signature.component.css']
})
export class CreateSignatureComponent implements OnInit {

  signature = new Signature();

  constructor(private apiService: SchoolService) { }

  ngOnInit() {

  }

  saveSignature(signature: Signature) {
    console.log(signature);
    this.apiService.saveSignature(signature)
      .then(result => {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success'
        );
      }).catch(error => {
      Swal.fire('Oops...', 'Something went wrong!', 'error');
      console.log("Error", error);
    });
  }

}
