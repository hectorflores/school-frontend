import { Component, OnInit } from '@angular/core';
import {SchoolService} from '../../service/school.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-signatures',
  templateUrl: './signatures.component.html',
  styleUrls: ['./signatures.component.css']
})
export class SignaturesComponent implements OnInit {

  signatureList: any = {};

  constructor(private schoolService: SchoolService) { }

  ngOnInit() {
    this.getSignatures();
  }

  getSignatures() {
    Swal.fire({
      allowOutsideClick: false,
      title: 'Loading',
      icon: 'info',
    });
    Swal.showLoading();

    this.schoolService.getSignatures().subscribe(res => {
      this.signatureList = res;
      console.log(res);
      Swal.close();
    }, err => {
      console.log(err);
      Swal.fire('Oops...', 'Something went wrong!', 'error');
    });
  }

}
