import { Component, OnInit } from '@angular/core';
import {SchoolService} from '../../service/school.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css']
})
export class ProfessorsComponent implements OnInit {

  professorsList: any = {};

  constructor(private schoolService: SchoolService) { }

  ngOnInit() {
    this.getProfessors();
  }

  getProfessors() {
    Swal.fire({
      allowOutsideClick: false,
      title: 'Loading',
      icon: 'info',
    });
    Swal.showLoading();

    this.schoolService.getProfessors().subscribe(res => {
      this.professorsList = res;
      console.log(res);
      Swal.close();
    }, err => {
      console.log(err);
      Swal.fire('Oops...', 'Something went wrong!', 'error');
    });
  }
}
