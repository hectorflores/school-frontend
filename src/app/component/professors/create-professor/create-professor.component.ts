import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../../../service/school.service';
import Swal from "sweetalert2";
import { Professor } from '../../../model/professor.model';

@Component({
  selector: 'app-create-professor',
  templateUrl: './create-professor.component.html',
  styleUrls: ['./create-professor.component.css']
})
export class CreateProfessorComponent implements OnInit {

  professor = new Professor();

  constructor(private apiService: SchoolService) { }

  ngOnInit() {

  }

  saveProfessor(professor: Professor) {
    console.log(professor);
    this.apiService.saveProfessor(professor)
      .then(result => {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success'
        );
      }).catch(error => {
      Swal.fire('Oops...', 'Something went wrong!', 'error');
      console.log("Error", error);
    });
  }

}
