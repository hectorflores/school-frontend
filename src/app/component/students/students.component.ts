import { Component, OnInit } from '@angular/core';
import {SchoolService} from '../../service/school.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  studentList: any = {};

  constructor(private schoolService: SchoolService) { }

  ngOnInit() {
    this.getStudents();
  }

  getStudents() {
    Swal.fire({
      allowOutsideClick: false,
      title: 'Loading',
      icon: 'info',
    });
    Swal.showLoading();

    this.schoolService.getStudents().subscribe(res => {
      this.studentList = res;
      console.log(res);
      Swal.close();
    }, err => {
      console.log(err);
      Swal.fire('Oops...', 'Something went wrong!', 'error');
    });
  }
}
