import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {SchoolService} from '../../../service/school.service';
import Swal from 'sweetalert2';
import {Student} from '../../../model/student.model';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  student = new Student();

  constructor(private apiService: SchoolService) { }

  ngOnInit() {

  }

  saveStudent(student: Student) {
    console.log(student);
    this.apiService.saveStudent(student)
      .then(result => {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success'
        );
      }).catch(error => {
        Swal.fire('Oops...', 'Something went wrong!', 'error');
        console.log("Error", error);
    });
  }
}
