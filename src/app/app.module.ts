import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { NavbarComponent } from './component/shared/navbar/navbar.component';
import { StudentsComponent } from './component/students/students.component';
import { ProfessorsComponent } from './component/professors/professors.component';
import { SignaturesComponent } from './component/signatures/signatures.component';
import { CreateProfessorComponent } from './component/professors/create-professor/create-professor.component';
import { CreateSignatureComponent } from './component/signatures/create-signature/create-signature.component';
import { CreateStudentComponent } from './component/students/create-student/create-student.component';

import {SchoolService} from './service/school.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentsComponent,
    NavbarComponent,
    SignaturesComponent,
    ProfessorsComponent,
    CreateProfessorComponent,
    CreateSignatureComponent,
    CreateStudentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [SchoolService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
